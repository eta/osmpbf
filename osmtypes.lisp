(in-package :osmpbf)

(defclass node ()
  ((id :initarg :id
       :type :integer
       :accessor osm-obj-id
       :documentation "OSM node ID.")
   (tags :initarg :tags
         :accessor osm-obj-tags
         :documentation "Association list of tags associated with this node.")
   (lat :initarg :lat
        :type :double
        :accessor node-lat
        :documentation "Latitude in degrees.")
   (lon :initarg :lon
        :type :double
        :accessor node-lon
        :documentation "Longitude in degrees.")))

(defmethod print-object ((obj node) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (id tags lat lon) obj
      (format stream "#~A (~A, ~A) tags: ~A" id lat lon tags))))

(defclass way ()
  ((id :initarg :id
       :type :integer
       :accessor osm-obj-id
       :documentation "OSM way ID.")
   (tags :initarg :tags
         :accessor osm-obj-tags
         :documentation "Association list of tags associated with this way.")
   (refs :initarg :refs
         :accessor way-refs
         :documentation "List of node IDs contained in this way.")))

(defmethod print-object ((obj way) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (id tags refs) obj
      (format stream "#~A tags: ~A refs: ~A" id tags refs))))

(defclass relation ()
  ((id :initarg :id
       :type :integer
       :accessor osm-obj-id
       :documentation "OSM relation ID.")
   (tags :initarg :tags
         :accessor osm-obj-tags
         :documentation "Association list of tags associated with this way.")
   (members :initarg :members
            :accessor relation-members
            :documentation "Association list of relation members (mapping relation role to object).")))

(defmethod print-object ((obj relation) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (id tags members) obj
      (format stream "#~A tags: ~A members: ~A" id tags members))))

(defclass primitive-group ()
  ((nodes :initarg :nodes
          :initform nil
          :accessor group-nodes
          :documentation "List of nodes.")
   (ways :initarg :ways
         :initform nil
         :accessor group-ways
         :documentation "List of ways.")
   (relations :initarg :relations
         :initform nil
         :accessor group-relations
         :documentation "List of relations.")))

(defmethod print-object ((obj primitive-group) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (nodes ways relations) obj
      (format stream "nodes: ~A ways: ~A relations: ~A" nodes ways relations))))
