(in-package :osmpbf)

(defvar *osm-symbols-package* (find-package 'keyword)
  "The package where OSM tag names are interned. Defaults to `keyword'; use `nil` for the current package.")

(defvar *perform-tag-name-translation* t
  "Whether or not to translate OSM tag names to symbols. Defaults to `t'.")
(defvar *tag-value-translation-names* '()
  "Tag names where the tag *values* should also be translated to symbols. Defaults to `nil'.")

(defun tag-name-to-symbol (tag-name)
  "Translates an OSM tag name to a Lisp symbol, interning it in *OSM-SYMBOLS-PACKAGE*."
  (flet ((transform-name (ch) (case ch
                                (#\_ #\-)
                                (#\: #\/)
                                (t (char-upcase ch)))))
    (intern (map 'string #'transform-name tag-name) *osm-symbols-package*)))

(defun make-tag-cons (name value)
  "Return a cons cell for the tag NAME=VALUE, performing tag/name value translation if configured to."
  (if *perform-tag-name-translation*
      (let ((name (tag-name-to-symbol name)))
        (cons name
              (if (member name *tag-value-translation-names*)
                  (tag-name-to-symbol value)
                  value)))
      (cons name value)))
