;;;; OSM PBF file format reader.
;;;; Implemented according to the spec @ https://wiki.openstreetmap.org/wiki/PBF_Format
;;;;
;;;; an eta project <https://theta.eu.org/>

(in-package :osmpbf)

(deftype octet () '(unsigned-byte 8))

(defun read-buffer-of-length (stream length)
  "Reads LENGTH bytes out of STREAM, and returns an array containing these bytes.
If an EOF is encountered midway through reading, signals an END-OF-FILE condition."
  (let ((ret (make-array length :element-type 'octet)))
    (when (not (eq (read-sequence ret stream) length))
      (error 'end-of-file))
    ret))

(defun read-header-length (stream)
  "Reads the int4 header length of the next BlobHeader from STREAM and returns it."
  (nibbles:ub32ref/be (read-buffer-of-length stream 4) 0))

(defun parse-pb-object (vector class length)
  "Parse an object of class CLASS from the bytes stored in VECTOR."
  (let ((ret (make-instance class)))
    (pb:merge-from-array ret vector 0 length)
    ret))

(defun read-pb-object (stream class length)
  "Read an object of class CLASS from STREAM, reading LENGTH bytes."
  (let ((buffer (read-buffer-of-length stream length)))
    (parse-pb-object buffer class length)))

(defun read-blob-header (stream)
  "Read a BLOB-HEADER from STREAM."
  (let ((length (read-header-length stream)))
    (read-pb-object stream 'opb:blob-header length)))

(defun read-blob-and-return-data (stream length)
  "Read a BLOB, of length LENGTH bytes, from STREAM, and return the buffer of data contained therein, uncompressing it if necessary.
Returns the buffer's actual length as a second value."
  (let* ((blob (read-pb-object stream 'opb:blob length)))
    (with-slots ((raw opb:raw) (zlib-data opb:zlib-data) (raw-size opb:raw-size)) blob
      (values (cond
                ((> (length raw) 0) raw)
                ((> (length zlib-data) 0) (let ((ret (make-array raw-size :element-type 'octet)))
                                            (chipz:decompress ret 'chipz:zlib zlib-data)
                                            ret))
                (t (error "Blob uses unsupported or unknown compression format!")))
      raw-size))))

(define-condition invalid-osm-fileblock (condition)
  ((name :initarg name
         :accessor invalid-osm-fileblock-name))
  (:report (lambda (condit stream)
             (format stream "Invalid OSM fileblock type ~A"
                     (invalid-osm-fileblock-name condit)))))

(defun read-fileblock (stream)
  "Reads an OSM fileblock, of any type, from STREAM, and returns it.
May signal an END-OF-FILE condition."
  (let* ((header (read-blob-header stream))
         (type (pb:string-value (slot-value header 'opb:type)))
         (length (slot-value header 'opb:datasize))
         (class-to-use (cond
                         ((equal type "OSMHeader") 'opb:header-block)
                         ((equal type "OSMData") 'opb:primitive-block)
                         (t (error 'invalid-osm-fileblock :name type)))))
    (multiple-value-bind (blob-data blob-length)
        (read-blob-and-return-data stream length)
      (parse-pb-object blob-data class-to-use blob-length))))

(defun parse-string-table (st)
  "Take a STRING-TABLE object and convert it into, uh, an actual vector of strings."
  (map 'vector (lambda (x) (flexi-streams:octets-to-string x :external-format :utf-8)) (slot-value st 'opb:s)))

(defun transform-coordinate (value granularity offset)
  "Transforms the coordinate (lat/lon) stored in VALUE from the PBF format to a regular double, using the GRANULARITY and OFFSET provided."
  (* (expt 10.0d0 -9.0d0) (+ offset (* granularity value))))

(defun keys-vals-to-alist (keys vals string-table)
  "Create an alist of key/value pairs, using the indexes provided by KEYS and VALS into STRING-TABLE.
Translates the keys to symbols if *PERFORM-TAG-NAME-TRANSLATION* is T."
  (assert (eq (length keys) (length vals)) (keys vals) "KEYS-VALS-TO-ALIST needs keys and vals of equal length")
  (flet ((collect-fn (key val) (make-tag-cons (aref string-table key) (aref string-table val))))
    ;; XXX: This is kinda hacky, but CL:LOOP doesn't let you iterate over generic sequences.
    ;; We just do this stupid specialization here; we could use the ITERATE package, but
    ;; that pulls in another dependency for just this one case.
    (if (vectorp keys)
        (loop for key across keys
              for val across vals
              collect (collect-fn key val))
        (loop for key in keys
              for val in vals
              collect (collect-fn key val)))))

(defun delta-decode (input)
  "Reverses delta-encoding on the INPUT vector, returning a vector of the same size with regular decoded values."
  (let ((ret (make-array (length input) :element-type (array-element-type input))))
    (loop
      for i from 0 to (1- (length input))
      with last = 0
      do (progn
               (incf last (aref input i))
               (setf (aref ret i) last)))
    ret))

(defun packed-keys-vals-to-alists (packed string-table)
  "Create a list of alists of key/value pairs, using the packed indexes in PACKED into STRING-TABLE.
Translates the keys to symbols if *PERFORM-TAG-NAME-TRANSLATION* is T."
  (loop
    with idx = 0
    while (< idx (length packed))
    collect (loop for i from idx by 2
                  until (eq (aref packed i) 0)
                  collect (make-tag-cons (aref string-table (aref packed i)) (aref string-table (aref packed (+ i 1))))
                  finally (setf idx (1+ i)))))

(defvar *current-lat-offset* 0)
(defvar *current-lon-offset* 0)
(defvar *current-granularity* 100)
(defvar *current-string-table* #())

(defun make-node (id tags lat lon)
  "Make a NODE object, transforming the LAT and LON coordinates."
  (make-instance 'node :id id
                       :tags tags
                       :lat (transform-coordinate lat *current-granularity* *current-lat-offset*)
                       :lon (transform-coordinate lon *current-granularity* *current-lon-offset*)))

(defun parse-node (node)
  (with-slots ((id opb:id) (keys opb:keys) (vals opb:vals) (lat opb:lat) (lon opb:lon)) node
    (make-node id (keys-vals-to-alist keys vals *current-string-table*) lat lon)))

(defun parse-dense-nodes (dense)
  (with-slots ((id opb:id) (lat opb:lat) (lon opb:lon) (keys-vals opb:keys-vals)) dense
      (loop for id across (delta-decode id)
            for lat across (delta-decode lat)
            for lon across (delta-decode lon)
            for tags in (packed-keys-vals-to-alists keys-vals *current-string-table*)
            collect (make-node id tags lat lon))))

(defun parse-way (way)
  (with-slots ((id opb:id) (keys opb:keys) (vals opb:vals) (refs opb:refs)) way
    (make-instance 'way
                   :id id
                   :tags (keys-vals-to-alist keys vals *current-string-table*)
                   :refs (delta-decode refs))))

(defun parse-relation-member-type (rmt)
  (ecase rmt
    (0 :node)
    (1 :way)
    (2 :relation)))

(defun parse-relation-members (roles memids types)
  (loop for role across roles
        for memid across memids
        for type across types
        collect (cons (aref *current-string-table* role) (cons (parse-relation-member-type type) memid))))

(defun parse-relation (rel)
  (with-slots ((id opb:id) (keys opb:keys) (vals opb:vals) (roles-sid opb:roles-sid) (memids opb:memids) (types opb:types)) rel
    (make-instance 'relation
                   :id id
                   :tags (keys-vals-to-alist keys vals *current-string-table*)
                   :members (parse-relation-members roles-sid memids types))))

(defun parse-primitive-block (primblock)
  (with-slots ((st opb:stringtable) (pgrps opb:primitivegroup) (granularity opb:granularity) (lato opb:lat-offset) (lono opb:lon-offset)) primblock
    (let ((*current-lat-offset* lato)
          (*current-lon-offset* lono)
          (*current-string-table* (parse-string-table st))
          (*current-granularity* granularity))
      (loop for pg across pgrps
            collect (with-slots ((nodes opb:nodes) (ways opb:ways) (relations opb:relations) (dense opb:dense)) pg
                      (make-instance 'primitive-group
                                     :ways (map 'list #'parse-way ways)
                                     :relations (map 'list #'parse-relation relations)
                                     :nodes (append
                                             (map 'list #'parse-node nodes)
                                             (if dense (parse-dense-nodes dense) nil))))))))

(defun get-next-primitive-groups (stream &key (eof-error-p t))
  "Get the next batch of PRIMITIVE-GROUP objects from the PBF octet stream STREAM.
STREAM must have element type (UNSIGNED-BYTE 8), otherwise an error will be signalled.

If EOF-ERROR-P is false, returns NIL if an end-of-file condition is reached."
  (assert (subtypep (stream-element-type stream) '(unsigned-byte 8)) ()
          "Can only read OSM data from streams with :ELEMENT-TYPE (UNSIGNED-BYTE 8)")
  (handler-case
      (parse-primitive-block
       (loop do
         (let ((blk (read-fileblock stream)))
           (etypecase blk
             (opb:primitive-block (return blk))
             (opb:header-block nil)))))
    (end-of-file (c) (when eof-error-p
                       (error c)))))
