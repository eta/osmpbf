(defpackage osmpbf
  (:use :cl)
  (:export :way :node :relation :primitive-group
           :read-fileblock :invalid-osm-fileblock
           :*current-lat-offset* :*current-lon-offset*
           :*current-granularity* :*current-string-table*
           :parse-primitive-block :get-next-primitive-groups
           :osm-obj-id :osm-obj-tags :node-lat :node-lon
           :way-refs :relation-members :group-nodes
           :group-ways :group-relations :*osm-symbols-package*
           :*perform-tag-name-translation* :tag-name-to-symbol
           :*tag-value-translation-names*))

(defpackage osmpbf/protobuf-inner
  (:nicknames :opb))
