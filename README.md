# `osmpbf`, the Lisp OSM PBF format reader

## What is this?

This library lets you read [OpenStreetMap][osm] map data encoded in [PBF format][opbf] in
the [Common Lisp][cl] programming language, for whatever mapping-related needs you might
have.

[osm]: https://www.openstreetmap.org/
[opbf]: https://wiki.openstreetmap.org/wiki/PBF_Format
[cl]: https://common-lisp.net/

## Implementation status

Basic support for reading nodes, ways, and relations is currently there (and it might even
be somewhat performant; who knows!). Node metadata information is, however, not decoded yet.

## How to use

### Installation instructions

1. Download the library, and put it somewhere ASDF can find it. (For example, `git clone`-ing the repo into `~/common-lisp/` should do the trick.)
2. Install the dependencies `com.google.base`, `protobuf`, `chipz`, `nibbles` and `flexi-streams`. (If [Quicklisp][ql] is installed, running `(ql:quickload :osmpbf)` should do this, and load the library as well!)
3. If you didn't do it with Quicklisp above, run `(asdf:load-system :osmpbf)` and watch the compiler output scroll by.
4. ???
5. Profit!

[ql]: https://www.quicklisp.org/beta/

### Basic usage

Basically, you open your PBF file, specifying something like `:direction :input :element-type (unsigned-byte 8)`, and pass the resulting stream to `get-next-primitive-groups`, repeating until you hit the end of the file.

```lisp
CL-USER> (with-open-file (file "sample.pbf"
                               :direction :input
                               :element-type '(unsigned-byte 8))
           (osmpbf:get-next-primitive-groups file))
(#<OSMPBF:PRIMITIVE-GROUP nodes: (#<NODE #653970877 (51.7636027d0, -0.22875700000000002d0) tags: NIL>
                                  #<NODE #647105170 (51.76359050000001d0, -0.23446450000000002d0) tags: NIL>
                                  #<NODE #672663476 (51.7657492d0, -0.2290703d0) tags: NIL>
                                  #<NODE #241806356 (51.7689451d0, -0.23266170000000003d0) tags: NIL>
                                  ... snip ...)
```

The `:element-type` bit is important; if you leave it out, it'll yell at you.
To figure out how to actually get useful things from the data returned, have a look at
the goodies in the `osmtypes.lisp` file, and their associated documentation.

On that note...

### Documentation

Is currently in the form of docstrings. Use the Source, Luke!

## License

MIT
