(defsystem "osmpbf"
  :name "osmpbf"
  :description "Library to read OpenStreetMap PBF-encoded files."
  :version "0.0.1"
  :author "eta <https://theta.eu.org>"
  :license "MIT"
  :depends-on ("com.google.base" "protobuf" "chipz" "flexi-streams" "nibbles")
  :serial t
  :components
  ((:file "packages")
   (:file "fileformat")
   (:file "osmformat")
   (:file "osmtypes")
   (:file "tag-translation")
   (:file "osmpbf")))
